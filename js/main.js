// Primeira aula de introdução

/* var nome = " Deus JohnBrow";

var frase = "Japao e a Melhor Seleção do Mundo "
console.log("Bem vindo " + nome);

// PROP REPLACE ALTERA STRING DENTRO DELE
console.log(frase.replace("Japao","Brasil"))
//  toUpperCase deixa o texto todo em maiusculo
console.log(frase.toUpperCase());
//  toLowerCase deixa o texto em minusculo
console.log(frase.toLowerCase());
*/


// Aula 2 Com Arrays e Dicionarios

/*var list = ["laranja", "macã", "pera"];
console.log(list);
// por indice do array
console.log(list[1]);

// prop PUSH adiciona mais um elemento no array
list.push("Limão");
console.log(list);

// e o elemento POP e para tirar o ultimo elemento
list.pop();
console.log(list);

// lista os elementos do array
console.log(list.length);

// Lista os elementos de forma reversa
console.log(list.reverse());

// Listar a primeira palavra do primeiro inice do array
console.log(list.toString()[0]);

// Listar todos indices em forma de string
console.log(list.toString());

// listar com um separador string entre as palavras
console.log(list.join(" - "));


// usando Dicionario
var fruta = {nome:"Maçã", cor:"Vermelha"};
console.log(fruta);


// Uma Lista de Dicionarios
var frutas = [
    {nome:"Maçã", cor:"Vermelha"},
    {nome:"Laranja", cor:"Laranja"},
    {nome:"Pera", cor:"Amarela"},
    {nome:"Uva", cor:"Roxa"},

];
console.log(frutas);
*/